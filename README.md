# weather app

#### Simple Weather App By Vanilla Js
Get information of  Weather API - [OpenWeatherMap](https://openweathermap.org/api)

**[Click To See](https://mohammadyousefvand.github.io/Weather-App/)**

پشتیبانی از زبان فارسی برای جستجوی شهر ها

## Features
- Area Information
- Feels Like temp
- Wind direction
- clock
- Temperature
- Maximum Temperature
- Minimum Temperature
- Humidity
- Wind Speed
- Sunrise
- Sunset
-  Icon

![Screenshot from 2022-02-09 16-45-56](https://user-images.githubusercontent.com/91375726/153209041-ebd27b82-77f6-490f-91bc-f6fd43153dae.png)
